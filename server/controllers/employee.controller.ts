import { NextFunction, Request, Response } from 'express';
import { EmployeeService } from '../services/employee.service';

export class EmployeesController {
    private static employeeService: EmployeeService = new EmployeeService();

    // Create an employee
    static async create(req: Request, res: Response, next: NextFunction) {
        const { firstName, lastName } = req.body;
        try {
            const employee = await EmployeesController.employeeService.createEmployee(firstName, lastName);
            res.status(201).json(employee);
        } catch (err) {
            // res.status(500).json({ message: err.message });
            next(err)
        }
    }

    // Get a list of all employees
    static async getAll(req: Request, res: Response, next: NextFunction) {
        try {
            const employees = await EmployeesController.employeeService.getAllEmployees();
            res.json(employees);
        } catch (err) {
            // res.status(500).json({ message: err.message });
            next(err)
        }
    }

    // Add vacations to an employee
    static async addVacations(req: Request, res: Response, next: NextFunction) {
        const { id } = req.params;
        const { startDate, endDate, comment } = req.body;
        try {
            const employee = await EmployeesController.employeeService.addVacationsToEmployee(id, startDate, endDate, comment);
            res.json(employee);
        } catch (err) {
            next(err)
        }
    }

    // Get the details of an employee, vacations included
    static async get(req: Request, res: Response, next: NextFunction) {
        const { id } = req.params;
        try {
            const employee = await EmployeesController.employeeService.getEmployee(id);
            res.json(employee);
        } catch (err) {
            // res.status(500).json({ message: err.message });
            next(err)
        }
    }

    // Return a list of employees which are currently off
    static async getOff(req: Request, res: Response, next: NextFunction) {
        const { startDate, endDate } = req.query;
        try {
            const employees = await EmployeesController.employeeService.getEmployeesOffInPeriod(new Date(startDate as string), new Date(endDate as string));
            res.json(employees);
        } catch (err) {
            // res.status(500).json({ message: err.message });
            next(err)
        }
    }
}
