import { randomUUID } from "crypto";

export class Vacation {
    public id: string;
    public startDate: Date;
    public endDate: Date;
    public comment: string;

    constructor(startDate: Date, endDate: Date, comment: string) {
        this.id = randomUUID(); // uniqueness is not guaranteed.
        this.startDate = startDate;
        this.endDate = endDate;
        this.comment = comment;
    }
}