import { randomUUID } from "crypto";
import { Vacation } from "./vacation";

export class Employee {
    public id: string;
    public firstName: string;
    public lastName: string;
    public vacations: Vacation[];

    constructor(firstName: string, lastName: string) {
        this.id = randomUUID(); // uniqueness is not guaranteed.
        this.firstName = firstName;
        this.lastName = lastName;
        this.vacations = [];
    }
}