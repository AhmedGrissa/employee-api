import HttpException from '../exceptions/http-exception';
import { Employee } from '../models/employee';
import { Vacation } from '../models/vacation';
import HTTP_STATUS from 'http-status';
export class EmployeeService {
    private _employees: Employee[];

    constructor() {
        this._employees = [];
    }

    get employees() {
        return this._employees;
    }

    set employees(employees: Employee[]) {
        this._employees = employees;
    }

    // Create an employee
    createEmployee(firstName: string, lastName: string): Employee {
        if (!firstName || !lastName) throw new HttpException(HTTP_STATUS.BAD_REQUEST, 'Bad request !');
        const employee = new Employee(firstName, lastName);
        this._employees.push(employee);
        return employee;
    }

    // Get a list of all employees
    getAllEmployees(): Employee[] {
        return this._employees;
    }

    // Add vacations to an employee
    addVacationsToEmployee(id: string, startDate: Date, endDate: Date, comment: string): Employee {
        if (!id || !startDate || !endDate) throw new HttpException(HTTP_STATUS.BAD_REQUEST, 'Bad request !');
        const employee = this._employees.find(e => e.id === id);
        if (!employee) {
            throw new HttpException(HTTP_STATUS.NOT_FOUND, 'Employee not found');
        }
        const vacation = new Vacation(startDate, endDate, comment);
        employee.vacations.push(vacation);
        return employee;
    }

    // Get the details of an employee, vacations included
    getEmployee(id: string): Employee {
        if (!id) throw new HttpException(HTTP_STATUS.BAD_REQUEST, 'Bad request !');
        const employee = this._employees.find(e => e.id === id);
        if (!employee) {
            throw new HttpException(HTTP_STATUS.NOT_FOUND, 'Employee not found');
        }
        return employee;
    }

    // Return a list of employees which are currently off
    getEmployeesOffInPeriod(startDate: Date, endDate: Date): Employee[] {
        if (!startDate || !endDate) throw new HttpException(HTTP_STATUS.BAD_REQUEST, 'Bad request !');
        return this._employees.filter(e => {
            const activeVacations = e.vacations.filter(v => {
                return v.startDate <= endDate && v.endDate >= startDate;
            });
            return activeVacations.length > 0;
        });
    }
}
