import { randomUUID } from 'crypto'
import HttpException from '../exceptions/http-exception'
import { Employee } from '../models/employee'
import { Vacation } from '../models/vacation'
import { EmployeeService } from './employee.service'

const id1 = randomUUID();
const id2 = randomUUID();
const id3 = randomUUID();
const id4 = randomUUID();
let employeeService = new EmployeeService()
let employee1 = new Employee('Adam','Smith');
employee1.id = id1;
let employee2 = new Employee('Stan', 'Smith');
employee2.id = id2;
let employee3 = new Employee('Mickey', 'Mouse');
employee3.id = id3;
let employee4 = new Employee('Dan', 'Smith');
employee4.id = id4;
const vacationA = new Vacation(new Date('2022-12-20'), new Date('2022-12-27'), 'Vacances de Noel');
const vacationB = new Vacation(new Date('2022-08-01'), new Date('2022-08-20'), 'Vacances d\'été');
const vacationC = new Vacation(new Date('2022-08-10'), new Date('2022-08-25'), 'Vacances d\'été');

describe('Should run all tests > ', () => {

    describe('Should test createEmployee > ', () => {
        beforeEach(() => {
            employeeService.employees = [];
        });
        it('should be able to correctly create an employee', () => {
            const createdEmployee = employeeService.createEmployee(employee1.firstName, employee1.lastName);
            expect(createdEmployee).toHaveProperty('firstName', employee1.firstName);
            expect(createdEmployee).toHaveProperty('lastName', employee1.lastName);
            expect(createdEmployee).toHaveProperty('id');
        });
        it('should be able to create another employee', () => {
            const createdEmployee = employeeService.createEmployee(employee1.firstName, employee1.lastName);
            expect(createdEmployee).toHaveProperty('firstName', employee1.firstName);
            expect(createdEmployee).toHaveProperty('lastName', employee1.lastName);
            expect(createdEmployee).toHaveProperty('id');
            expect(employeeService.employees).toHaveLength(1);
        });
        it('should be able to create multiple employees', () => {
            const createdEmployee1 = employeeService.createEmployee(employee1.firstName, employee1.lastName);
            const createdEmployee2 = employeeService.createEmployee(employee2.firstName, employee2.lastName);
            expect(createdEmployee1).toHaveProperty('firstName', employee1.firstName);
            expect(createdEmployee1).toHaveProperty('lastName', employee1.lastName);
            expect(createdEmployee1).toHaveProperty('id');
            expect(createdEmployee2).toHaveProperty('firstName', employee2.firstName);
            expect(createdEmployee2).toHaveProperty('lastName', employee2.lastName);
            expect(createdEmployee2).toHaveProperty('id');
            expect(employeeService.employees).toHaveLength(2);
        });
        it('should throw an error when missing param', () => {
            // @ts-ignore
            expect(() => employeeService.createEmployee(employee1.firstName)).toThrow('Bad request !');
        })

        afterAll(() => {
            employeeService.employees = [];
        })
    })

    describe('Should test getAllEmployees > ', () => {
        afterEach(() => {
            employeeService.employees = [
                employee3,
                employee4
            ];
        });
        afterAll(() => {
            employeeService.employees = [];
        })
        it('Should return empty list', () => {
            expect(employeeService.getAllEmployees()).toMatchObject([]);
        });
        it('Should return all employees correctly', () => {
            expect(employeeService.getAllEmployees()).toMatchObject([employee3, employee4]);
        });
    });

    describe('should test addVacationsToEmployee > ', () => {
        beforeEach(() => {
            employeeService.employees = [
                employee3,
                employee4
            ];
        });
        afterAll(() => {
            employeeService.employees = [];
            employee4.vacations = [];
        })
        it('Should add first vacation to employee', () => {
            const vacation = new Vacation(new Date('2022-01-01'), new Date('2022-01-10'), 'Vacances de Noel');
            const modifiedEmployee = employeeService.addVacationsToEmployee(employee3.id, vacation.startDate, vacation.endDate, vacation.comment);
            expect(modifiedEmployee.vacations[0]).toHaveProperty('startDate', vacation.startDate);
            expect(modifiedEmployee.vacations[0]).toHaveProperty('endDate', vacation.endDate);
            expect(modifiedEmployee.vacations[0]).toHaveProperty('comment', vacation.comment);
        });

        it('Should throw an error on bad arguments', () => {
            const vacation = new Vacation(new Date('2022-01-01'), new Date('2022-01-10'), 'Vacances de Noel');
            // @ts-ignore
            expect(() => employeeService.addVacationsToEmployee(employee3.id, vacation.startDate)).toThrow('Bad request !');
        });

        it('Should add multiple vacation to same employee', () => {
            let modifiedEmployee = employeeService.addVacationsToEmployee(employee4.id, vacationA.startDate, vacationA.endDate, vacationA.comment);
            modifiedEmployee = employeeService.addVacationsToEmployee(employee4.id, vacationB.startDate, vacationB.endDate, vacationB.comment);
            expect(modifiedEmployee.vacations[0]).toHaveProperty('startDate', vacationA.startDate);
            expect(modifiedEmployee.vacations[0]).toHaveProperty('endDate', vacationA.endDate);
            expect(modifiedEmployee.vacations[0]).toHaveProperty('comment', vacationA.comment);

            expect(modifiedEmployee.vacations[1]).toHaveProperty('startDate', vacationB.startDate);
            expect(modifiedEmployee.vacations[1]).toHaveProperty('endDate', vacationB.endDate);
            expect(modifiedEmployee.vacations[1]).toHaveProperty('comment', vacationB.comment);
        });

    })

    describe('should get an employee correctly > ', () => {
        beforeEach(() => {
            employeeService.employees = [
                employee4
            ];
        });
        afterAll(() => {
            employeeService.employees = [];
            employee4.vacations = [];
        })
        it('Should throw an error on bad arguments', () => {
            // @ts-ignore
            expect(() => employeeService.getEmployee()).toThrow('Bad request !')
        });

        it('Should get employee', () => {
            expect(employeeService.getEmployee(employee4.id)).toMatchObject(employee4);
        });
    });

    describe('should get employees off in given period', () => {
        beforeEach(() => {
            employee1 = { ...employee1, vacations: [vacationA] };
            employee2 = { ...employee1, vacations: [vacationC] };
            employee3 = { ...employee1, vacations: [vacationB] };
            employee4 = { ...employee1, vacations: [vacationA, vacationB] };
            employeeService.employees = [employee1, employee2, employee3, employee4];
        });

        it('Should throw an error on bad arguments', () => {
            // @ts-ignore
            expect(() => employeeService.getEmployeesOffInPeriod()).toThrow('Bad request !')
            // @ts-ignore
            expect(() => employeeService.getEmployeesOffInPeriod(new Date())).toThrow('Bad request !')
        });

        it('should return employees who are off during a period', () => {
            expect(employeeService.getEmployeesOffInPeriod(new Date('2022-08-10'), new Date('2022-08-25'))).toMatchObject([employee2, employee3, employee4]);
        })

    })
})