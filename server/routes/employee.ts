import express, { NextFunction, Request, Response } from 'express';
import { createValidator } from 'express-joi-validation';
import { EmployeesController as controller } from '../controllers/employee.controller';
import { addVacationsToEmployee, createEmployee, getEmployeesOffInPeriod } from '../middlewares/validation/employee.schema';

const router = express.Router();
const validator = createValidator()

// Create an employee
router.post('/', validator.body(createEmployee), controller.create);

// Get a list of all employees
router.get('/', controller.getAll);

// Add vacations to an employee
router.patch('/:id/vacations', validator.body(addVacationsToEmployee), controller.addVacations);

// Return a list of employees which are currently off
router.get('/off', validator.query(getEmployeesOffInPeriod), controller.getOff);

// Get the details of an employee, vacations included
router.get('/:id', controller.get);

export default router;
