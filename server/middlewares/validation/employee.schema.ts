import Joi, { CustomHelpers } from "joi";

export const createEmployee = Joi.object({
    firstName: Joi.string().required(),
    lastName: Joi.string().required(),
})

const validateDates = (input: { startDate: string | Date, endDate: string | Date }, helpers: CustomHelpers) => {
    // invert startDate with endDate if startDate > endDate
    if (new Date(input.startDate) > new Date(input.endDate)) {
        return {...input, startDate: input.endDate, endDate: input.startDate }
    }
    return input;
}

export const addVacationsToEmployee = Joi.object({
    startDate: Joi.date().required(),
    endDate: Joi.date().required(),
    comment: Joi.string()
}).custom(validateDates)


export const getEmployeesOffInPeriod = Joi.object({
    startDate: Joi.date().required(),
    endDate: Joi.date().required(),
}).custom(validateDates)
