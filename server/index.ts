import express from 'express';
import employeeRouter from './routes/employee';
import bodyParser from 'body-parser';
import errorMiddleware from './middlewares/error.middleware';
import swaggerUi from "swagger-ui-express";
import * as swaggerDocument from "./swagger.json";

const PORT = 3000; // add a config folder
const app = express();

app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/employee', employeeRouter);

app.use((errorMiddleware))

app.listen(PORT, () => {
  console.log(`Employee vacation tracker API listening on port ${PORT} !`);
});