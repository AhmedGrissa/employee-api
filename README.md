 -  To start server in dev:
	
	`npm run dev`
- To build server:
    
    `npm run build`

- to run tests:

	`  npm run test`

- to build docker image:
`docker build -t http-employee-test .`

- to run container from image:
` docker run -p 3000:3000 http-employee-test` 