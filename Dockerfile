# Use an official Node.js runtime as the base image
FROM node:16-alpine

# Set the working directory inside the container
WORKDIR /app

# Copy the package.json and package-lock.json files to the container
COPY package*.json ./

# Install the dependencies
RUN npm install

# Copy the rest of the application files to the container
COPY . .

# Build the TypeScript files
RUN npm run build

# Specify the command to run when the container starts
CMD ["npm", "start"]

# Expose the default port used by the application
EXPOSE 3000
